# README #

** reflect4j **

** A set of utilities to facilitate the work with reflection **

** version 0.1 **

### Setting up ###

*Just clone repository:* 

`git clone https://bitbucket.org/cache-kz/reflect4j.git`

*To build project use:*
 
`mvn install`

*To run tests:*

`mvn test`