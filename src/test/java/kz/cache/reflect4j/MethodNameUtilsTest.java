package kz.cache.reflect4j;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import kz.cache.reflect4j.util.MethodNameUtils;

public class MethodNameUtilsTest {

   public MethodNameUtilsTest() {
   }

   @Test
   public void extratFieldName() {
      assertEquals("a", MethodNameUtils.extractFieldName("getA"));
      assertEquals("foo", MethodNameUtils.extractFieldName("getFoo"));

      assertEquals("a", MethodNameUtils.extractFieldName("isA"));
      assertEquals("foo", MethodNameUtils.extractFieldName("isFoo"));

      assertEquals("a", MethodNameUtils.extractFieldName("setA"));
      assertEquals("foo", MethodNameUtils.extractFieldName("setFoo"));
   }

   @Test
   public void formatFieldName() {
      assertEquals("getA", MethodNameUtils.buildAccessorName("a"));
      assertEquals("getFoo", MethodNameUtils.buildAccessorName("foo"));

      assertEquals("isA", MethodNameUtils.buildBooleanAccessorName("a"));
      assertEquals("isFoo", MethodNameUtils.buildBooleanAccessorName("foo"));

      assertEquals("setA", MethodNameUtils.buildMutatorName("a"));
      assertEquals("setFoo", MethodNameUtils.buildMutatorName("foo"));
   }

}
