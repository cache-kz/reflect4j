package kz.cache.reflect4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import kz.cache.reflect4j.model.B;

public class BeanComparatorTest {

   protected BeanManager<B> bm;

   @Before
   public void init() {
      bm = new BeanManager<>(B.class);
   }

   @Test
   public void comparePrimitives() {

      // tests for field "d". it is Integer (implements Comparable)
      B b1 = new B();
      B b2 = new B();
      B b3 = new B();
      B b4 = new B();

      BeanComparator<B> bcAsc = new BeanComparator<>(bm, "a");
      BeanComparator<B> bcDesc = new BeanComparator<>(bm, "a", false);

      b1.a = 8;
      b2.a = 3;
      b3.a = -7;
      b4.a = 5;

      List<B> testlist = Arrays.asList(b1, b2, b3, b4);

      Collections.sort(testlist, bcAsc);
      assertEquals(-7, bm.getInteger("a", testlist.get(0)));
      assertEquals(3, bm.getInteger("a", testlist.get(1)));
      assertEquals(5, bm.getInteger("a", testlist.get(2)));
      assertEquals(8, bm.getInteger("a", testlist.get(3)));

      Collections.sort(testlist, bcDesc);
      assertEquals(8, bm.getInteger("a", testlist.get(0)));
      assertEquals(5, bm.getInteger("a", testlist.get(1)));
      assertEquals(3, bm.getInteger("a", testlist.get(2)));
      assertEquals(-7, bm.getInteger("a", testlist.get(3)));
   }

   @Test
   public void compareComparable() {

      // tests for field "d". it is Integer (implements Comparable)
      B b1 = new B();
      B b2 = new B();
      B b3 = new B();
      B b4 = new B();

      BeanComparator<B> bcAsc = new BeanComparator<>(bm, "d");
      BeanComparator<B> bcDesc = new BeanComparator<>(bm, "d", false);

      b1.d = 8;
      b2.d = 3;
      b3.d = -7;
      b4.d = 5;

      List<B> testlist = Arrays.asList(b1, b2, b3, b4);

      Collections.sort(testlist, bcAsc);
      assertEquals(-7, bm.getInteger("d", testlist.get(0)));
      assertEquals(3, bm.getInteger("d", testlist.get(1)));
      assertEquals(5, bm.getInteger("d", testlist.get(2)));
      assertEquals(8, bm.getInteger("d", testlist.get(3)));

      Collections.sort(testlist, bcDesc);
      assertEquals(8, bm.getInteger("d", testlist.get(0)));
      assertEquals(5, bm.getInteger("d", testlist.get(1)));
      assertEquals(3, bm.getInteger("d", testlist.get(2)));
      assertEquals(-7, bm.getInteger("d", testlist.get(3)));
   }

   @Test
   public void compareNoncomparable() {
      //Do not use BeanComparator with noncomparable fields
      boolean errorThrown = false;
      try {
         //Error will be thrown this line
         BeanComparator<B> bc = new BeanComparator<>(bm, "class");

         bc.compare(null, null);
      } catch (RuntimeException e) {
         assertEquals("Trying to use bean comparator on noncomparable field class", e.getMessage());
         errorThrown = true;
      }
      assertTrue(errorThrown);
   }

}
