package kz.cache.reflect4j.model;

import kz.cache.reflect4j.annotation.Ignore;

public class A {

   public int a;

   protected int b;

   private int c;

   @Ignore
   public int newField;

   public A() {
   }

   public int getC() {
      return c;
   }

   public void setC(int c) {
      this.c = c;
   }

   @Ignore
   public int getNewMethod() {
      return 0;
   }

}
