package kz.cache.reflect4j.model;

import java.util.List;
import java.util.Map;

public class C {

   public B fldB;

   public List<B> fldBsList;

   public B[] fldBsArray;

   public Map<String, B> fldBsMap;

}
