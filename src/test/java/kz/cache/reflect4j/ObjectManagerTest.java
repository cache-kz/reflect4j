package kz.cache.reflect4j;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.Test;

import kz.cache.reflect4j.model.B;
import kz.cache.reflect4j.model.C;

public class ObjectManagerTest {

   protected static ObjectManager reader = null;

   @BeforeClass
   public static void init() {
      reader = new ObjectManager();
   }

   @Test
   public void readSimpleProperty() {
      C c = new C();
      c.fldB = new B();
      c.fldB.a = 10;

      assertEquals(10, reader.getValue("fldB.a", c));
   }

   @Test
   public void readListProperty() {
      C c = new C();
      c.fldBsList = new ArrayList<>();
      c.fldBsList.add(new B());
      c.fldBsList.get(0).d = 7;
      c.fldBsList.add(new B());
      c.fldBsList.get(1).d = 9;

      assertEquals(7, reader.getValue("fldBsList[0].d", c));
      assertEquals(9, reader.getValue("fldBsList[1].d", c));
   }

   @Test
   public void readArrayProperty() {
      C c = new C();
      c.fldBsArray = new B[2];
      c.fldBsArray[0] = new B();
      c.fldBsArray[0].d = 8;
      c.fldBsArray[1] = new B();
      c.fldBsArray[1].d = 12;

      assertEquals(8, reader.getValue("fldBsArray[0].d", c));
      assertEquals(12, reader.getValue("fldBsArray[1].d", c));
   }

   @Test
   public void readMapProperty() {
      C c = new C();
      c.fldBsMap = new HashMap<>();
      c.fldBsMap.put("stringA", new B());
      c.fldBsMap.put("stringB", new B());
      c.fldBsMap.get("stringA").d = 8;
      c.fldBsMap.get("stringB").d = 15;

      assertEquals(8, reader.getValue("fldBsMap(stringA).d", c));
      assertEquals(15, reader.getValue("fldBsMap(stringB).d", c));
      assertEquals(null, reader.getValue("fldBsMap(stringAas).?d", c));
   }

   @Test
   public void writeProperty() {
      C c = new C();
      c.fldBsMap = new HashMap<>();
      B b = new B();
      c.fldBsMap.put("stringA", b);

      reader.setValue("fldBsMap(stringA).d", c, 10);
      assertEquals(10, reader.getValue("fldBsMap(stringA).d", c));

      reader.setValue("fldBsMap(stringAsz).?d", c, 10);
      assertEquals(null, reader.getValue("fldBsMap(stringAsz).?d", c));
   }

   @Test
   public void writeArrayProperty() {
      C c = new C();
      c.fldBsArray = new B[3];
      B b = new B();
      b.d = 8;

      reader.setValue("fldBsArray[1]", c, b);
      assertEquals(8, reader.getValue("fldBsArray[1].d", c));
   }

   @Test
   public void writeListProperty() {
      C c = new C();
      B b = new B();
      b.d = 8;

      reader.setValue("fldBsList", c, new ArrayList<B>());
      reader.setValue("fldBsList[]", c, b);
      assertEquals(8, reader.getValue("fldBsList[0].d", c));
   }

   @Test
   public void writeMapProperty() {
      C c = new C();
      c.fldBsMap = new HashMap<>();
      B b = new B();
      b.d = 8;

      reader.setValue("fldBsMap(stringA)", c, b);
      assertEquals(8, reader.getValue("fldBsMap(stringA).d", c));
   }

}
