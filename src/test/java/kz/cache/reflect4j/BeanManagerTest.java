package kz.cache.reflect4j;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import kz.cache.reflect4j.model.A;
import kz.cache.reflect4j.model.B;

public class BeanManagerTest {

   protected BeanManager<B> bm;

   @Before
   public void init() {
      bm = new BeanManager<>(B.class);
   }

   @Test
   public void checkFieldsVisibility() {

      // All VISIBLE field names in class B: [a, c, d, f, g, class]
      // field b, inherited from class A and field e is invisible (has protected scope and has no getters or setters)
      // field h is invisible because it static
      // field class is visible because each class has method getClass(). it is not static field B.class
      // fields newField and newMethod marked as @Ignore
      assertEquals(6, bm.getFieldNames().size());
      assertTrue(bm.getFieldNames().containsAll(Arrays.asList("a", "c", "d", "f", "g", "class")));

      // All READABLE field names in class B: [a, c, d, f, class]
      assertEquals(5, bm.getFieldNamesReadable().size());
      assertTrue(bm.getFieldNamesReadable().containsAll(Arrays.asList("a", "c", "d", "f", "class")));

      // All WRITEABLE field names in class B: [a, c, d, g]
      assertEquals(4, bm.getFieldNamesWriteable().size());
      assertTrue(bm.getFieldNamesWriteable().containsAll(Arrays.asList("a", "c", "d", "g")));

      // All READ ONLY (have no mutator) field names in class B: [f, class]
      assertEquals(2, bm.getFieldNamesReadOnly().size());
      assertTrue(bm.getFieldNamesReadOnly().containsAll(Arrays.asList("f", "class")));

      // All WRITE ONLY (have no accessor) field names in class B: [g]
      assertEquals(1, bm.getFieldNamesWriteOnly().size());
      assertTrue(bm.getFieldNamesWriteOnly().contains("g"));

      // All READ WRITE (have mutator and accessor) field names in class B: [a, c, d]
      assertEquals(3, bm.getFieldNamesReadWrite().size());
      assertTrue(bm.getFieldNamesReadWrite().containsAll(Arrays.asList("a", "c", "d")));
   }

   @Test
   public void getValue() {
      B b1 = new B();
      b1.a = 5;
      b1.d = 10;
      b1.setC(8);

      assertEquals(5, bm.getValue("a", b1));
      assertNotEquals(6, bm.getValue("a", b1));

      assertEquals(10, bm.getValue("d", b1));
      assertNotEquals(12, bm.getValue("d", b1));

      assertEquals(8, bm.getValue("c", b1));
      assertNotEquals(12, bm.getValue("c", b1));

      assertEquals(B.class, bm.getValue("class", b1));
      assertNotEquals(A.class, bm.getValue("class", b1));
      //Class A is subclass of B
      assertTrue(A.class.isAssignableFrom((Class< ? >) bm.getValue("class", b1)));

      boolean errorThrown = false;
      try {
         bm.getValue("b", b1);
      } catch (NoSuchMethodError e) {
         assertEquals("Unable to find accessor for field b in class B", e.getMessage());
         errorThrown = true;
      }
      assertTrue(errorThrown);

   }

   @Test
   public void getValueTyped() {
      B b1 = new B();
      b1.a = 5;
      b1.d = 10;

      //getting value
      assertEquals(5, bm.getInteger("a", b1));
      //Casting int to Integer
      assertEquals(new Integer(5), bm.getIntegerObj("a", b1));
      //just test
      assertNotEquals(6, bm.getInteger("a", b1));

      //Casting Integer to int
      assertEquals(10, bm.getInteger("d", b1));
      //getting value
      assertEquals(new Integer(10), bm.getIntegerObj("d", b1));
      //just test
      assertNotEquals(new Integer(6), bm.getIntegerObj("d", b1));

      //getting value which is not initialized (default int value is 0)
      assertEquals(0, bm.getInteger("c", b1));

      boolean errorThrown = false;
      try {
         // Class cast exception should be thrown (static cast integer to long)
         bm.getLong("a", b1);
      } catch (ClassCastException e) {
         errorThrown = true;
      }
      assertTrue(errorThrown);
   }

   @Test
   public void copyObject() {
      // All READ WRITE field names in class B: [a, c, d] (only this fields will be copied)
      B b1 = new B();
      b1.a = 5;
      b1.d = 10;
      b1.setC(12);

      B b2 = bm.copy(b1);

      assertEquals(bm.getValue("a", b1), bm.getValue("a", b2));
      assertEquals(bm.getValue("c", b1), bm.getValue("c", b2));
      assertEquals(bm.getValue("d", b1), bm.getValue("d", b2));

   }

}
