package kz.cache.reflect4j;

import java.lang.reflect.Array;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class which manages objects properties including inner object properties
 *
 * @author Anatoliy
 *
 */
public class ObjectManager {

   protected static final String PATTERN_ARRAY_GETTER = "([^\\s\\[\\]\\(\\)]+)\\[([0-9]+)\\]";
   protected static final String PATTERN_ARRAY_SETTER = "([^\\s\\[\\]\\(\\)]+)\\[([0-9]*)\\]";
   protected static final String PATTERN_MAP = "([^\\s\\[\\]\\(\\)]+)\\((.+)\\)";

   protected Pattern arrayPatternGetter = Pattern.compile(PATTERN_ARRAY_GETTER);
   protected Pattern arrayPatternSetter = Pattern.compile(PATTERN_ARRAY_SETTER);
   protected Pattern mapPattern = Pattern.compile(PATTERN_MAP);

   protected Map<Class< ? >, BeanManager<Object>> managers = new HashMap<>();

   public ObjectManager() {
   }

   /**
    * gets object's field value by expression
    *
    * @param fieldsExpression - expression of field to access
    * <br/><b>ex:</b><br />
    *    - getValue("fieldName", object);<br/>
    *    - getValue("fieldNameArray[index]", object);<br/>
    *    - getValue("fieldNameArray[index].innerProperty", object);<br/>
    *    - getValue("fieldNameArray[index].innerProperty.subProperty.etc", object);<br/>
    *    - getValue("fieldNameList[index]", object);<br/>
    *    - getValue("fieldNameMap(stringKeyValue)", object);<br/>
    * <b>where:</b><br />
    * <b>object</b> - is the object which field's value should be accessed<br />
    * <b>fieldNameArray</b> - name of field of array type<br />
    * <b>fieldNameList</b> - name of field of <link java.util.List>java.util.List</link> type<br />
    * <b>fieldNameMap</b> - name of field of <link java.util.Map>java.util.Map</link> type <b>with key of type java.lang.String</b><br />
    *
    * @param obj - object which field's value should be accessed
    * @return
    * value, found by field expression
    */
   public Object getValue(String fieldsExpression, Object obj) {
      List<String> fieldExpressions = parseExpression(fieldsExpression);
      return getValue(fieldExpressions, obj);
   }

   protected Object getValue(List<String> fieldExpressions, Object obj) {
      Object out = obj;

      for (String fieldExpression : fieldExpressions) {
         boolean avoidNpe = false;
         if (fieldExpression.startsWith("?")) {
            fieldExpression = fieldExpression.substring(1, fieldExpression.length());
            avoidNpe = true;
         }

         if (avoidNpe && out == null) {
            break;
         }

         Matcher matcher = arrayPatternGetter.matcher(fieldExpression);
         if (matcher.matches()) {
            out = getFieldValue(matcher.group(1), out);

            if (List.class.isInstance(out)) {
               out = ((List< ? >) out).get(Integer.parseInt(matcher.group(2)));
            } else if (Object[].class.isInstance(out)) {
               out = ((Object[]) out)[Integer.parseInt(matcher.group(2))];
            } else {
               out = null;
            }
            continue;
         } else {
            matcher = mapPattern.matcher(fieldExpression);

            if (matcher.matches()) {
               out = getFieldValue(matcher.group(1), out);
               if (Map.class.isInstance(out)) {
                  out = ((Map< ? , ? >) out).get(matcher.group(2));
               } else {
                  out = null;
               }
               continue;
            } else {
               out = getFieldValue(fieldExpression, out);
            }
         }
      }

      return out;
   }

   /**
    * gets object's field value by expression
    *
    * @param fieldsExpression - expression of field to access
    * <br/><b>ex:</b><br />
    *    - setValue("fieldName", object);<br />
    *    - setValue("fieldNameArray[index].innerProperty", object, value);<br />
    *    - setValue("fieldNameArray[index].innerProperty.subProperty.etc", object, value); - <b>sets value to property etc</b><br />
    *    - setValue("fieldNameList[]", object, value); - <b>adds element to the end of List. Not applicable to arrays</b><br />
    *    - setValue("fieldNameList[index]", object, value); - <b>sets value to position of index</b><br />
    *    - setValue("fieldNameMap(stringKeyValue)", object, value);<br />
    * <b>where:</b><br />
    * <b>object</b> - is the object which field's value should be accessed<br />
    * <b>value</b> - value to set<br />
    * <b>fieldNameArray</b> - name of field of array type<br />
    * <b>fieldNameList</b> - name of field of <link java.util.List>java.util.List</link> type<br />
    * <b>fieldNameMap</b> - name of field of <link java.util.Map>java.util.Map</link> type <b>with key of type java.lang.String</b><br />
    *
    * @param obj - object which field's value should be accessed
    * @param value - value to set
    */
   @SuppressWarnings({"unchecked", "rawtypes"})
   public void setValue(String fieldsExpression, Object obj, Object value) {
      Object tmpValue = obj;
      List<String> fieldExpressions = parseExpression(fieldsExpression);

      if (fieldExpressions.size() > 1) {
         List<String> fieldExpressionsBeg = fieldExpressions.subList(0, fieldExpressions.size() - 1);
         tmpValue = getValue(fieldExpressionsBeg, tmpValue);
      }

      String fieldExpression = fieldExpressions.get(fieldExpressions.size() - 1);
      boolean avoidNpe = false;

      if (fieldExpression.startsWith("?")) {
         fieldExpression = fieldExpression.substring(1, fieldExpression.length());
         avoidNpe = true;
      }

      if (!(avoidNpe && tmpValue == null)) {
         Matcher matcher = arrayPatternSetter.matcher(fieldExpression);
         if (matcher.matches()) {
            tmpValue = getFieldValue(matcher.group(1), tmpValue);

            if (List.class.isInstance(tmpValue)) {
               String indexStr = matcher.group(2);
               if ("".equals(indexStr)) {
                  ((List) tmpValue).add(value);
               } else {
                  ((List) tmpValue).set(Integer.parseInt(matcher.group(2)), value);
               }
            } else if (Object[].class.isInstance(tmpValue)) {
               Array.set(tmpValue, Integer.parseInt(matcher.group(2)), value);
            } else {
               throw new InvalidParameterException(String.format("Unsupported collection type for field %s", fieldExpression));
            }
         } else {
            matcher = mapPattern.matcher(fieldExpression);
            if (matcher.matches()) {
               tmpValue = getFieldValue(matcher.group(1), tmpValue);
               if (Map.class.isInstance(tmpValue)) {
                  ((Map) tmpValue).put(matcher.group(2), value);
               } else {
                  tmpValue = null;
               }
            } else {
               setFieldValue(fieldExpression, tmpValue, value);
            }
         }
      }

   }

   protected List<String> parseExpression(String fieldsExpression) {
      List<String> out = new ArrayList<>();

      String[] splitted = fieldsExpression.split("\\.");
      StringBuilder currentField = new StringBuilder();

      for (String item : splitted) {
         currentField.append(item);
         if (currentField.indexOf("(") >= 0 && currentField.indexOf(")") < 0) {
            currentField.append('.');
         } else {
            out.add(currentField.toString());
            currentField.setLength(0);
         }
      }

      return out;
   }

   protected Object getFieldValue(String fieldName, Object obj) {
      Object out = null;
      Class< ? > type = obj.getClass();
      BeanManager<Object> manager = getManager(type);
      out = manager.getValue(fieldName, obj);
      return out;
   }

   protected void setFieldValue(String fieldName, Object obj, Object value) {
      Class< ? > type = obj.getClass();
      BeanManager<Object> manager = getManager(type);
      manager.setValue(fieldName, obj, value);
   }

   protected BeanManager<Object> getManager(Class< ? > type) {
      BeanManager<Object> manager = managers.get(type);
      if (manager == null) {
         manager = new BeanManager<>(type);
         managers.put(type, manager);
      }
      return manager;
   }

}
