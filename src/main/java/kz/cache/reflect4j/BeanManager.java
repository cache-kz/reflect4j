package kz.cache.reflect4j;

import static java.util.Objects.requireNonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import kz.cache.reflect4j.annotation.Ignore;
import kz.cache.reflect4j.model.Accessor;
import kz.cache.reflect4j.model.FieldAccessor;
import kz.cache.reflect4j.model.FieldMutator;
import kz.cache.reflect4j.model.MethodAccessor;
import kz.cache.reflect4j.model.MethodMutator;
import kz.cache.reflect4j.model.Mutator;
import kz.cache.reflect4j.util.MethodNameUtils;

/**
 * Class which manages objects properties
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class BeanManager<T> {

   protected Map<String, Accessor<T>> accessors = new HashMap<>();
   protected Map<String, Mutator<T>> mutators = new HashMap<>();

   protected Class< ? extends T> type;

   public Class< ? extends T> getType() {
      return type;
   }

   public BeanManager(Class< ? extends T> type) {
      this.type = type;

      init();
   }

   /**
    * @return Set of field names, which have either accessor or mutator or both
    */
   public Set<String> getFieldNames() {
      Set<String> out = new HashSet<>();
      out.addAll(accessors.keySet());
      out.addAll(mutators.keySet());
      return out;
   }

   /**
    * @return Set of field names, which have accessor
    */
   public Set<String> getFieldNamesReadable() {
      return accessors.keySet();
   }

   /**
    * @return Set of field names, which have mutator
    */
   public Set<String> getFieldNamesWriteable() {
      return mutators.keySet();
   }

   /**
    * @return Set of field names, which have accessor, and have no mutator
    */
   public Set<String> getFieldNamesReadOnly() {
      Set<String> out = new HashSet<>();
      out.addAll(accessors.keySet());
      out.removeAll(mutators.keySet());
      return out;
   }

   /**
    * @return Set of field names, which have mutator, and have no accessor
    */
   public Set<String> getFieldNamesWriteOnly() {
      Set<String> out = new HashSet<>();
      out.addAll(mutators.keySet());
      out.removeAll(accessors.keySet());
      return out;
   }

   /**
    * @return Set of field names, which have accessor and mutator
    */
   public Set<String> getFieldNamesReadWrite() {
      Set<String> out = new HashSet<>();
      out.addAll(accessors.keySet());
      out.retainAll(mutators.keySet());
      return out;
   }

   public Map<String, Accessor<T>> getAccessors() {
      return accessors;
   }

   public Map<String, Mutator<T>> getMutators() {
      return mutators;
   }

   public Accessor<T> getAccessor(String fieldName) {
      return accessors.get(fieldName);
   }

   public Mutator<T> getMutator(String fieldName) {
      return mutators.get(fieldName);
   }

   /**
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    */
   public Object getValue(String fieldName, T obj) {
      Accessor<T> accessor = accessors.get(fieldName);
      if (accessor == null) {
         throw new NoSuchMethodError(String.format("Unable to find accessor for field %s in class %s", fieldName, getType().getSimpleName()));
      }
      return accessor.getValue(obj);
   }

   /**
    * Returns field value as primitive byte
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive byte
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public byte getByte(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (byte) value;
   }

   /**
    * Returns field value as byte wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as byte wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Byte getByteObj(String fieldName, T obj) {
      return (Byte) getValue(fieldName, obj);
   }

   /**
    * Returns field value as primitive short
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive short
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public short getShort(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (short) value;
   }

   /**
    * Returns field value as short wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as short wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Short getShortObj(String fieldName, T obj) {
      return (Short) getValue(fieldName, obj);
   }

   /**
    * Returns field value as primitive integer
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive integer
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public int getInteger(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (int) value;
   }

   /**
    * Returns field value as integer wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as integer wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Integer getIntegerObj(String fieldName, T obj) {
      return (Integer) getValue(fieldName, obj);
   }

   /**
    * Returns field value as primitive long
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive long
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public long getLong(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (long) value;
   }

   /**
    * Returns field value as byte wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as byte wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Long getLongObj(String fieldName, T obj) {
      return (Long) getValue(fieldName, obj);
   }

   /**
    * Returns field value as primitive float
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive float
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public float getFloat(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (float) value;
   }

   /**
    * Returns field value as float wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as float wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Float getFloatObj(String fieldName, T obj) {
      return (Float) getValue(fieldName, obj);
   }

   /**
    * Returns field value as primitive float
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as primitive float
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public double getDouble(String fieldName, T obj) {
      Object value = getValue(fieldName, obj);
      return value == null ? 0 : (double) value;
   }

   /**
    * Returns field value as double wrapper
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as double wrapper
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public Double getDoubleObj(String fieldName, T obj) {
      return (Double) getValue(fieldName, obj);
   }

   /**
    * Returns field value as string
    * @param fieldName - field name
    * @param obj - object
    * @return
    * value of object field as string
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    * @throws ClassCastException unchecked exception when field type can not be casted to return type of method
    */
   public String getString(String fieldName, T obj) {
      return (String) getValue(fieldName, obj);
   }

   /**
    * @param fieldName - field name
    * @param obj - object
    * @param value - new value of field
    * @throws NoSuchMethodError unchecked exception when field mutator not found
    */
   public void setValue(String fieldName, T obj, Object value) {
      Mutator<T> mutator = mutators.get(fieldName);
      if (mutator == null) {
         throw new NoSuchMethodError(String.format("Unable to find mutator for field %s in class %s", fieldName, getType().getSimpleName()));
      }
      mutator.setValue(obj, value);
   }

   /**
    * @param fieldName - field name
    * @return - type of readable field
    * @throws NoSuchMethodError unchecked exception when field accessor not found
    */
   public Class< ? > getFieldType(String fieldName) {
      Accessor<T> accessor = accessors.get(fieldName);
      if (accessor == null) {
         throw new NoSuchMethodError(String.format("Unable to find accessor for field %s in class %s", fieldName, getType().getSimpleName()));
      }
      return accessor.getFieldType();
   }

   /**
    * Creates new instance of object
    * @return
    * new instance of object. Class of object should have <b>default public constructor (constructor with no arguments)</b>
    * @throws NoSuchMethodError unchecked exception when default public constructor was not found
    * @throws NullPointerException if source object is null
    */
   public T create() {
      T out = null;
      try {
         out = getType().newInstance();
      } catch (InstantiationException | IllegalAccessException e) {
         throw new NoSuchMethodError(String.format("Unable to copy object. Default constructor of type %s not found", getType().getSimpleName()));
      }
      return out;
   }

   /**
    * Copies one object into another
    * @param source - source object which should be copied
    * @return
    * copy of <b>source</b> object. Class of object should have <b>default public constructor (constructor with no arguments)</b>
    * @throws NoSuchMethodError unchecked exception when default public constructor was not found
    * @throws NullPointerException if source object is null
    */
   public T copy(T source) {
      T destination = create();
      return copy(source, destination);
   }

   /**
    * Copies <b>source</b> object into <b>destination</b>
    * @param source - source object which should be copied
    * @param destination - object which fields should be updated
    * @return
    * destination object
    * @throws NullPointerException if either source or destination object is null
    */
   public T copy(T source, T destination) {
      requireNonNull(source);
      requireNonNull(destination);

      for (String fieldName : getFieldNamesReadWrite()) {
         Accessor<T> accessor = accessors.get(fieldName);
         Mutator<T> mutator = mutators.get(fieldName);
         Object value = accessor.getValue(source);
         mutator.setValue(destination, value);
      }
      return destination;
   }

   protected void init() {
      Set<String> fieldNames = new HashSet<>();
      Map<String, Field> fieldsRW = new HashMap<>();
      Map<String, Field> fieldsRO = new HashMap<>();

      for (Field field : type.getFields()) {
         if (!Modifier.isStatic(field.getModifiers())) {
            Annotation annotation = field.getAnnotation(Ignore.class);
            if (annotation != null) {
               continue;
            }
            fieldNames.add(field.getName());
            fieldsRO.put(field.getName(), field);
            if (!Modifier.isFinal(field.getModifiers())) {
               fieldsRW.put(field.getName(), field);
            }
         }
      }

      Map<String, Method> accessorMethods = new HashMap<>();
      Map<String, Method> mutatorMethods = new HashMap<>();

      for (Method method : type.getMethods()) {
         if (!Modifier.isStatic(method.getModifiers())) {
            Annotation annotation = method.getAnnotation(Ignore.class);
            if (annotation != null) {
               continue;
            }
            if (method.getParameters().length == 0) {
               if (method.getName().length() > 3 && method.getName().startsWith(MethodNameUtils.GETTER_PREFIX)) {
                  String fieldName = MethodNameUtils.extractFieldName(method.getName());
                  fieldNames.add(fieldName);
                  accessorMethods.put(fieldName, method);
               } else if (method.getName().length() > 2 && method.getName().startsWith(MethodNameUtils.GETTER_PREFIX_BOOL)) {
                  String fieldName = MethodNameUtils.extractFieldName(method.getName());
                  fieldNames.add(fieldName);
                  accessorMethods.put(fieldName, method);
               }
            } else if (method.getParameters().length == 1) {
               if (method.getName().length() > 3 && method.getName().startsWith(MethodNameUtils.SETTER_PREFIX)) {
                  String fieldName = MethodNameUtils.extractFieldName(method.getName());
                  fieldNames.add(fieldName);
                  mutatorMethods.put(fieldName, method);
               }
            }
         }
      }

      for (String fieldName : fieldNames) {
         Method accessor = accessorMethods.get(fieldName);
         if (accessor != null) {
            accessors.put(fieldName, new MethodAccessor<T>(accessor));
         } else {
            Field field = fieldsRO.get(fieldName);
            if (field != null) {
               accessors.put(fieldName, new FieldAccessor<T>(field));
            }
         }

         Method mutator = mutatorMethods.get(fieldName);
         if (mutator != null) {
            mutators.put(fieldName, new MethodMutator<T>(mutator));
         } else {
            Field field = fieldsRW.get(fieldName);
            if (field != null) {
               mutators.put(fieldName, new FieldMutator<T>(field));
            }
         }
      }
   }

}
