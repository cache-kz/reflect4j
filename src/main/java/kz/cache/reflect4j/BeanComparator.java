package kz.cache.reflect4j;

import java.util.Comparator;

/**
 * Comparator implementation which compares objects using some field values
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class BeanComparator<T> implements Comparator<T> {

   protected BeanManager<T> manager;
   protected String fieldName;
   protected boolean sortAsc;

   public BeanComparator(Class<T> type, String fieldName) {
      this(new BeanManager<>(type), fieldName, true);
   }

   public BeanComparator(Class<T> type, String fieldName, boolean sortAsc) {
      this(new BeanManager<>(type), fieldName, sortAsc);
   }

   public BeanComparator(BeanManager<T> manager, String fieldName) {
      this(manager, fieldName, true);
   }

   public BeanComparator(BeanManager<T> manager, String fieldName, boolean sortAsc) {
      this.manager = manager;
      this.fieldName = fieldName;
      this.sortAsc = sortAsc;
      Class< ? > fieldType = manager.getFieldType(fieldName);

      //primitives casts to wrapper types, so they are comparable
      if (!fieldType.isPrimitive() && !Comparable.class.isAssignableFrom(fieldType)) {
         throw new RuntimeException(String.format("Trying to use bean comparator on noncomparable field %s", fieldName));
      }
   }

   @SuppressWarnings({"unchecked", "rawtypes"})
   @Override
   public int compare(T o1, T o2) {
      int out = 0;
      Object v1 = manager.getValue(fieldName, o1);
      Object v2 = manager.getValue(fieldName, o2);

      if (v1 == null && v2 == null) {
         out = 0;
      } else if (v1 == null) {
         out = sortAsc ? 1 : -1;
      } else if (v2 == null) {
         out = sortAsc ? -1 : 1;
      } else {
         out = sortAsc ? ((Comparable) v1).compareTo(v2) : -((Comparable) v1).compareTo(v2);
      }
      return out;
   }

}
