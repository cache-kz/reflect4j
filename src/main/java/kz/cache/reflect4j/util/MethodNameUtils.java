package kz.cache.reflect4j.util;

/**
 * Util class for field and method name processing
 *
 * @author Anatoliy
 */
public class MethodNameUtils {

   public static final String GETTER_PREFIX = "get";
   public static final String GETTER_PREFIX_BOOL = "is";
   public static final String SETTER_PREFIX = "set";

   private MethodNameUtils() {
   }

   /**
    * builds accessor name by field name
    * <br/><b>ex:</b>
    * <br/>buildAccessorName("foo") returns "getFoo"
    *
    * @param fieldName - field name
    * @return - accessor name
    */
   public static String buildAccessorName(String fieldName) {
      return formatMethodName("get", fieldName);
   }

   /**
    * builds accessor name by field name
    * <br/><b>ex:</b>
    * <br/>buildBooleanAccessorName("foo") returns "isFoo"
    *
    * @param fieldName - field name
    * @return - accessor name
    */
   public static String buildBooleanAccessorName(String fieldName) {
      return formatMethodName("is", fieldName);
   }

   /**
    * builds mutator name by field name
    * <br/><b>ex:</b>
    * <br/>buildMutatorName("foo") returns "setFoo"
    *
    * @param fieldName - field name
    * @return - accessor name
    */
   public static String buildMutatorName(String fieldName) {
      return formatMethodName("set", fieldName);
   }

   /**
    * extracts field name from accessor/mutator(gettter/setter) name
    * <br/><b>ex:</b>
    * <br/>extractFieldName("getFoo") returns "foo"
    * <br/>extractFieldName("setFoo") returns "foo"
    * <br/>extractFieldName("isFoo") returns "foo"
    *
    * @param methodName - method name
    * @return - field name
    */
   public static String extractFieldName(String methodName) {
      StringBuilder sb = new StringBuilder();

      if (methodName.indexOf("get") == 0 || methodName.indexOf("set") == 0) {
         sb.append(methodName.substring(3, 4).toLowerCase());
         if (methodName.length() > 4) {
            sb.append(methodName.substring(4));
         }
      } else if (methodName.indexOf("is") == 0) {
         sb.append(methodName.substring(2, 3).toLowerCase());
         if (methodName.length() > 3) {
            sb.append(methodName.substring(3));
         }
      }
      return sb.toString();
   }

   protected static String formatMethodName(String methodType, String fieldName) {
      StringBuilder sb = new StringBuilder();
      sb.append(methodType);
      sb.append(fieldName.substring(0, 1).toUpperCase());
      if (fieldName.length() > 1) {
         sb.append(fieldName.substring(1));
      }
      return sb.toString();
   }

}
