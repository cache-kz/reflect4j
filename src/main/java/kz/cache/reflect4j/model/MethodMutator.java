package kz.cache.reflect4j.model;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * {@link kz.cache.reflect4j.model.Mutator Mutator} implementation which provides access via field's <b>setter</b>
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class MethodMutator<T> implements Mutator<T> {

   protected final Method method;

   public MethodMutator(Method method) {
      requireNonNull(method, "Unable to construct MethodMutator with empty method");

      this.method = method;
   }

   @Override
   public void setValue(T obj, Object value) {
      try {
         method.invoke(obj, value);
      } catch (IllegalArgumentException e) {
         throw new RuntimeException("Trying to set property value of invalid type");
      } catch (IllegalAccessException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalAccessException while set value via MethodMutator");
      } catch (InvocationTargetException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("InvocationTargetException while set value via MethodMutator");
      }
   }

}
