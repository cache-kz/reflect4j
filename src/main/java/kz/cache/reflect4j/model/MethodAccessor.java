package kz.cache.reflect4j.model;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * {@link kz.cache.reflect4j.model.Accessor Accessor} implementation which provides access via field's <b>getter</b>
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class MethodAccessor<T> implements Accessor<T> {
   protected final Method method;

   public MethodAccessor(Method method) {
      requireNonNull(method, "Unable to construct MethodAccessor with empty method");

      this.method = method;
   }

   @Override
   public Object getValue(T obj) {
      try {
         return method.invoke(obj);
      } catch (IllegalAccessException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalAccessException while get value via MethodAccessor");
      } catch (InvocationTargetException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("InvocationTargetException while get value via MethodAccessor");
      } catch (IllegalArgumentException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalArgumentException while get value via MethodAccessor");
      }
   }

   @Override
   public Class< ? > getFieldType() {
      return method.getReturnType();
   }

}
