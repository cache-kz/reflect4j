package kz.cache.reflect4j.model;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.Field;

/**
 * {@link kz.cache.reflect4j.model.Mutator Mutator} implementation which provides access via object's <b>field</b>
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class FieldMutator<T> implements Mutator<T> {

   protected final Field field;

   public FieldMutator(Field field) {
      requireNonNull(field, "Unable to construct FieldMutator with empty field");

      this.field = field;
   }

   @Override
   public void setValue(T obj, Object value) {
      try {
         field.set(obj, value);
      } catch (IllegalArgumentException e) {
         throw new RuntimeException("Trying to set property value of invalid type");
      } catch (IllegalAccessException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalAccessException while set value via FieldMutator");
      }
   }

}
