package kz.cache.reflect4j.model;

/**
 * Interface which defines object's property accessor
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public interface Accessor<T> {

   public Object getValue(T obj);

   public Class< ? > getFieldType();

}
