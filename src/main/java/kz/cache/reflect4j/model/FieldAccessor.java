package kz.cache.reflect4j.model;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.Field;

/**
 * {@link kz.cache.reflect4j.model.Accessor Accessor} implementation which provides access via object's <b>field</b>
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public class FieldAccessor<T> implements Accessor<T> {

   protected final Field field;

   public FieldAccessor(Field field) {
      requireNonNull(field, "Unable to construct FieldAccessor with empty field");

      this.field = field;
   }

   @Override
   public Object getValue(T obj) {
      try {
         return field.get(obj);
      } catch (IllegalArgumentException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalArgumentException while getting value via FieldAccessor");
      } catch (IllegalAccessException e) {
         //This exception should never be thrown (while using BeanManager)
         throw new RuntimeException("IllegalAccessException while getting value via FieldAccessor");
      }
   }

   @Override
   public Class< ? > getFieldType() {
      return field.getType();
   }

}
