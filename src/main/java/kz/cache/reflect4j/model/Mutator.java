package kz.cache.reflect4j.model;

/**
 * Interface which defines object's property accessor
 *
 * @author Anatoliy
 *
 * @param <T> - type of object
 */
public interface Mutator<T> {

   public void setValue(T obj, Object value);

}
